# ffmpeg-wrapper #

Basic HLS stream creator, using ffmpeg-wrapper module

### How to use ###

	var hlsStreamer = require('ffmpeg-hls');
	
	var streamer = hlsStreamer({
    	input: "rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov",
    	output: "/my/web/root/testlist.m3u8",
    	params : {
    		"vcodec": "libx264",
    		"acodec": "libvo_aacenc",
    		"map": ["0:0", "0:1"]
    	},
    	started: function(playlist) {
    		console.log("STARTED", playlist);
    	},
    	complete: function(error, complete) {
    		console.log("COMPLETE", error, complete);
    	},
    	progress: function(progress) {
    		console.log("PROGRESS", progress);
    	}
    });
    
    streamer.start();

	// stop after 20 seconds (20 seconds from the tiem this function is triggered, not 20 seconds into the stream)
    setTimeout(function() {
        streamer.stop(function(error, done) {
            console.log("STOP", error, done);
        });
    }, 20000);