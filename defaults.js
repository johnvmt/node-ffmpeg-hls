var config = {};

config.debug = true;

config.params = {
	"f": "hls",
	"hls_flags": "delete_segments",
	"segment_time": 2,
	"segment_list_size": 4,
	"segment_format": "mpegts",
	"flags": "-global_header",
	"vcodec": "libx264",
	"acodec": "libmp3lame"
};

module.exports = config;