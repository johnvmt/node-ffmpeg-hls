module.exports = function(config) {
	return new HlsEncoder(config);
};

var path = require('path');
var fs = require('fs');
var ffmpeg = require('ffmpeg-wrapper');
var Utils = require(path.join(__dirname, 'Utils.js'));
var configDefaults = require(path.join(__dirname, 'defaults.js'));

function HlsEncoder(config) {
	this.startOnSetEnd = false;
	this.setInProgress = false;
	this.setConfig(config);
}

HlsEncoder.prototype.setConfig = function(config) {
	var self = this;

	var ffmpegParams = Utils.objectMerge(configDefaults.params, config.params); // merge ffmpeg params separately

	self.config = Utils.objectMerge(configDefaults, config);
	self.config.params = ffmpegParams;
	self.encoder = self.getEncoder(self.config);

	// auto-map
	if(!(self.config.params.map instanceof Array)) {
		self.setInProgress = true; // so that encode won't start
		self.config.params.map = [];
		self.firstStreams(function(error, firstStreams) {
			if(!error && typeof firstStreams === 'object' && firstStreams != null) {
				if(typeof firstStreams.video === 'number')
					self.config.params.map.push("0:" + String(firstStreams.video));
				if(typeof firstStreams.audio === 'number')
					self.config.params.map.push("0:" + String(firstStreams.audio));
			}
			self.encoder = self.getEncoder(self.config);
			self.setInProgress = false;
			if(self.startOnSetEnd) {
				self.startOnSetEnd = false;
				self.start();
			}
		});
	}
};

HlsEncoder.prototype.getConfig = function() {
	return this.config;
};

HlsEncoder.prototype.firstStreams = function(callback) {
	// get the first of each type of stream
	var firstStreams = {};
	this.probe(function(error, results) {
		if(!error && results.streams instanceof Array) {
			results.streams.forEach(function(stream) {
				if(typeof stream.codec_type === 'string' && typeof stream.index === 'number' && typeof firstStreams[stream.codec_type] === 'undefined')
					firstStreams[stream.codec_type] = stream.index;
			});
			callback(null, firstStreams);
		}
		else
			callback(error, null);
	});
};

HlsEncoder.prototype.start = function() {
	if(this.setInProgress) // getting map parameters
		this.startOnSetEnd = true;
	else
		this.encoder.start();
};

HlsEncoder.prototype.probe = function(callback) {
	this.encoder.probe(callback);
};

HlsEncoder.prototype.stop = function(callback) {
	// TODO delete encoder when stopping
	if(this.encoder)
		this.encoder.stop(callback);
	else
		callback("not_started", null);
};

HlsEncoder.prototype.getEncoder = function(config) {
	var started = false;
	return ffmpeg({
		input: config.input,
		output: config.output,
		params: config.params,
		debug: false,
		complete: function(error, complete) {
			if(config.complete)
				config.complete(error, complete);
		},
		progress: function(progress) {
			if(config.progress)
				config.progress(progress);

			if(!started && typeof config.started === "function") {
				try {
					fs.statSync(config.output); // check if playlist file exists yet
					started = true;
					config.started(config.output);
				}
				catch(error) { }
			}
		}
	});
};
